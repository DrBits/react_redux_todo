import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as todoActions from '../../actions';

import TodoTextField from '../../components/TodoTextField/TodoTextField';
import TodoList from '../../components/TodoList/TodoList';

import { MuiThemeProvider } from 'material-ui/styles';
import Paper from 'material-ui/Paper';

const styles = {
  container: {
    marginTop: 20,
    display: 'flex',
    justifyContent: 'center'
  },
  paper: {
    padding: 20,
    width: '50%'
  }
};

const App = ({ todos, actions }) =>
  <MuiThemeProvider>
    <div style={styles.container}>
      <Paper zDepth={2} style={styles.paper}>
        <TodoTextField actions={actions} />
        <TodoList todos={todos} actions={actions} />
      </Paper>
    </div>
  </MuiThemeProvider>;

const mapStateToProps = ({ todos }) => ({
  todos
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(todoActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
