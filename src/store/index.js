import { createStore, compose } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import localForage from 'localforage';
import reducers from '../reducers';

function configureStore() {
  const store = createStore(reducers, compose(autoRehydrate()));

  persistStore(store, { storage: localForage });

  return store;
}

export default configureStore();
