## Install dependencies

```
npm install
```

## Running development build

```
npm start
```

## Running production build

```
npm run build
```